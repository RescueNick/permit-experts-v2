---
class: bg-num1
---

  <div class="columns col-gapless">
    <div class="col-4 col-sm-12">
      <img class="img-responsive" src="/user/pages/01.home/_socialproof/Edmonton.png">
    </div>
    <div class="col-4 col-sm-12">
      <img class="img-responsive" src="/user/pages/01.home/_socialproof/Red-Deer.png">
    </div>
    <div class="col-4 col-sm-12">
      <img class="img-responsive" src="/user/pages/01.home/_socialproof/Calgary.png">
    </div>
  </div>
