---
class: bg-num0
---
 
  <h2 style="text-align:center">How It Works</h2>
  <div class="columns">
    <div class="col-4 col-sm-12 hiw">
      <h2>1</h2>
      <img style="max-width: 100px" src="/user/pages/01.home/_howitworks/laptop.png">
      <h6 class="text-green">Send Us Required Documents</h6>
      <p>After you accept our quote we will send you a list of the required documents for your project. Email them to us ASAP!</p>
    </div>
    <div class="col-4 col-sm-12 hiw">
      <h2>2</h2>
      <img style="max-width: 100px" src="/user/pages/01.home/_howitworks/contract.png">
      <h6 class="text-green">Preparation / Filing</h6>
      <p>After we receive all the requested documents we will prepare your applicatoin and have it submitted to the appropriate department in 2 business days.</p>
    </div>
    <div class="col-4 col-sm-12 hiw">
      <h2>3</h2>
      <img style="max-width: 100px" src="/user/pages/01.home/_howitworks/hands.png">
      <h6 class="text-green">Approval</h6>
      <p>Our Experienced team fills out the documents propery the first time resulting in faster approvals! We will let you know the moment your project is approved.</p>
    </div>
  </div>

