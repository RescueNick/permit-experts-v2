---
title: "Terms and Conditions"
slug: terms
---

#Permit Experts Terms and Conditions
######Last Updated on May 10th, 2018

1. The owner or its contractor is responsible to apply and obtain the electrical, mechanical and plumbing permits when required. Permit Experts is not responsible for the electrical, mechanical and plumbing review and compliance. All electrical, plumbing and mechanical information illustrated on the plans must be validated for compliance prior to initiating the work.

2. Once the local jurisdiction has approved the permit application and all payments have been received, Permit Experts will deliver the documentation to the client. Should revisions or additional support be required Permit Experts will invoice its services on an hourly basis at $100 per hour with a minimum fee of $75 per request.

3. Liability
The Parties hereby agree to limit the liability of the Consultant to the Client for any and all claims, losses, costs, damages or claim expenses of any kind or nature, including solicitor legal fees and expert-witness fees and related costs, arising from this Agreement so that the total liability of the Consultant to Client shall not exceed the total fee amount, as stated above, or the total fees actually paid, whichever is less. It is intended that this limitation apply to any and all liability or cause of action alleged or arising in connection with this Agreement.

4. Due Diligence
Permit Experts is not responsible for carrying out due diligence for existing non–complying uses or structures. Should a permit application trigger a review from the city and identifies non-compliances, Permit Experts will evaluate its ability to resolve the non-conforming issues and will, at its discretion, provide a revised quote for review and authorization.
Invoices and Outstanding accounts
Payment for all amounts billed shall be due and payable within seven (7) days from the date of invoice. The Consultant reserves the right to stop work on a project at any time should an account or invoice become outstanding beyond such seven (7) day period. Any unpaid invoice after the seven (7) day period will incur an administrative charge of $50 or 10%, whichever is greater, the charge will be applied to the total amount owing. A monthly interest rate of 2% will be applied monthly until full payment is received. Work can resume once the account is brought up-to-date at the discretion of Permit Experts. Payments shall be made to “Permit Experts.” Permit Experts will invoice for work completed prior to the permit submission. No formal application will be made prior to full payment of all outstanding invoices. No permit will be released prior to full payment of all outstanding invoices.
Performance of work

5. The Consultant will use reasonable care, skill, competence, and judgment in the performance of the services which are generally consistent with the professional standards for individuals providing similar services at the same time, in the same locale, and under similar circumstances. The planning process is complex and involves high risk. The Consultant will take reasonable steps to ensure the success of the project but does not take responsibility for any negative outcomes.

Disbursement fees
Any expenditures or disbursements (city fee, engineering review, surveying, etc.) made on behalf of the Client will be charged at Permit Experts’ cost plus 6%. (There is the possibility to directly pay for your permits rather than going through Permit Experts.)

6. The client and/or owner shall verify the drawings produced by Permit Experts. Any inconsistency shall be communicated to Permit Experts prior to submission for approval.

7. The plans prepared for the permit approval should not be used for unit counts, construction material dimensions or construction drawings.
Furnace and hot water tank locations to be verified by a qualified professional before installation or construction.

8. Respectful process. The Consultant will act with the outmost respect for the Client and every other consultant and authority involved in the process. The Consultant expects this respect to be mutual and Permit Experts reserves the right to stop work in the event that the relationship is not positive and/or is affecting the reputation of the company.
Timelines

9. Permit Experts is not responsible for delays in the application process resulting from the local jurisdiction review.

10. City Inspection
The client is responsible to call and schedule its inspections at the appropriate time during construction. Permit Experts is not responsible for the execution of the work in accordance to the drawings produced for the permit application nor is it responsible for additional requirement imposed by the inspectors.

11. Engineering and architectural requirements
Our fee proposal does not include the preparation of engineering and architectural drawings or review. Should drawings or review be required a cost estimate will be provided to you prior to initiating the work.
Termination of Contract

12. At all times, Permit Experts will have the right to terminate its services upon written notice to that effect. If Permit Experts terminates its services, it will render a final accounting covering all unbilled time, other charges and disbursements. The client shall pay this invoice and all other outstanding invoice in accordance to the payment terms.

13. Should the client choose to terminate this agreement an invoice will be issued for any outstanding time spent on that project up to the notice of termination from the client.

14. Copyright
All drawings and documents, produced under the terms of this agreement, are the property of Permit Experts. Unauthorized reproduction or distribution will be prosecuted.

15. Miscellaneous Terms
This Agreement supersedes all prior and contemporaneous agreements, assurances, representations, and communications between the Parties hereto.

No amendment or variation of the terms, conditions, warranties, covenants, agreements and undertakings set forth herein shall be of any force or effect unless the same shall be reduced to writing duly executed by all Parties hereto in the same manner and with the same formality as this Agreement is executed.
This Agreement shall be governed by, and be construed in accordance with, the laws of the Province of Alberta. The Parties consent to the venue and the jurisdiction of the Court of Queen's Bench of Alberta, Judicial District of Calgary, for any action commenced relating to this Agreement or the transactions contemplated hereby.

Time shall be of the essence of this Agreement.
All references to currency herein are references to Canadian Dollars.

No provision of this Agreement shall be deemed to be waived unless a waiver is in writing. Any waiver of any default committed by any of the Parties hereto in the observance or performance of any part of this Agreement shall not extend to or be taken in any manner to effect any other default.





















