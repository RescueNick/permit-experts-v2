<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => '/Users/nick/Dropbox/Development/Permit_Experts_v2/user/plugins/admin/blueprints/admin/pages/move.yaml',
    'modified' => 1525314735,
    'data' => [
        'form' => [
            'validation' => 'loose',
            'fields' => [
                'route' => [
                    'type' => 'parents',
                    'label' => 'PLUGIN_ADMIN.PARENT',
                    'classes' => 'fancy'
                ]
            ]
        ]
    ]
];
