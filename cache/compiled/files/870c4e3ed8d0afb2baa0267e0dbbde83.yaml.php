<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => '/var/www/html/permit-experts-v2/user/plugins/admin/blueprints/admin/pages/move.yaml',
    'modified' => 1525383596,
    'data' => [
        'form' => [
            'validation' => 'loose',
            'fields' => [
                'route' => [
                    'type' => 'parents',
                    'label' => 'PLUGIN_ADMIN.PARENT',
                    'classes' => 'fancy'
                ]
            ]
        ]
    ]
];
