<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => '/var/www/html/permit-experts-v2/user/config/site.yaml',
    'modified' => 1525383596,
    'data' => [
        'title' => 'Permit Experts',
        'author' => [
            'name' => 'Magnumpous',
            'email' => 'dev@magnumpous.com'
        ],
        'metadata' => [
            'description' => 'Hire Permit Experts to file all your building and renovation permits on your behalf to save money and time. Permits filed in 2 business days'
        ]
    ]
];
