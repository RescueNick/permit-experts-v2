<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => '/var/www/html/permit-experts-v2/user/accounts/dev-admin.yaml',
    'modified' => 1525389426,
    'data' => [
        'email' => 'nick@3leafshomes.com',
        'fullname' => 'Dev Admin',
        'title' => 'Administrator',
        'state' => 'enabled',
        'access' => [
            'admin' => [
                'super' => 'true',
                'login' => 'true',
                'cache' => 'true',
                'configuration' => 'true',
                'configuration_system' => 'true',
                'configuration_site' => 'true',
                'configuration_media' => 'true',
                'configuration_info' => 'true',
                'settings' => 'true',
                'pages' => 'true',
                'maintenance' => 'true',
                'statistics' => 'true',
                'plugins' => 'true',
                'themes' => 'true',
                'users' => 'true'
            ],
            'site' => [
                'login' => 'true'
            ]
        ],
        'hashed_password' => '$2y$10$PUjIDvYeN0EbCNIZgppG0ungXmr3.FCoWmAzAqqOYOtILygz/pQ6u',
        'language' => 'en',
        'login_attempts' => [
            
        ],
        'twofa_secret' => '6Z5SZARZVXMLP3S5SSCNTC7EGC4UMB2O',
        'twofa_enabled' => true
    ]
];
