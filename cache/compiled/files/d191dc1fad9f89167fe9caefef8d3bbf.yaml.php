<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => '/Users/nick/Dropbox/Development/Permit_Experts_v2/user/plugins/markdown-notices/markdown-notices.yaml',
    'modified' => 1525312761,
    'data' => [
        'enabled' => true,
        'built_in_css' => true,
        'level_classes' => [
            0 => 'yellow',
            1 => 'red',
            2 => 'blue',
            3 => 'green'
        ]
    ]
];
