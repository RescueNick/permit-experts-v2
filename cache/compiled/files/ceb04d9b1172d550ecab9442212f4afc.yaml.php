<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => '/var/www/html/permit-experts-v2/user/accounts/khoury.yaml',
    'modified' => 1525391207,
    'data' => [
        'email' => 'khoury@permitexperts.ca',
        'access' => [
            'admin' => [
                'login' => true,
                'super' => true
            ],
            'site' => [
                'login' => true
            ]
        ],
        'fullname' => 'Khoury Suhail',
        'title' => 'Owner',
        'state' => 'enabled',
        'hashed_password' => '$2y$10$GiAAvuztzooIuw7Z5zXvjulp.NpExHRpYPrySN/MVpzCW1iLME1KC'
    ]
];
