<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => '/var/www/html/permit-experts-v2/user/accounts/deidre.yaml',
    'modified' => 1525458932,
    'data' => [
        'email' => 'deidre@permitexperts.ca',
        'access' => [
            'admin' => [
                'super' => 'true',
                'login' => 'true'
            ],
            'site' => [
                'login' => 'true'
            ]
        ],
        'fullname' => 'Diedre',
        'title' => 'Admin',
        'state' => 'enabled',
        'hashed_password' => '$2y$10$1gVxPSsYjdwwyx9KfBPEKe9sEvZjB1vrD9mtBYqAqGZfrU6FUKG2y',
        'language' => 'en',
        'login_attempts' => [
            
        ],
        'twofa_secret' => 'ORG2GXAD5KHOJFHHNN4M4YNNTH4T75ZS',
        'twofa_enabled' => false
    ]
];
