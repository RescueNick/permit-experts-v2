<?php

/* modular/section.html.twig */
class __TwigTemplate_575cfe437d7aedae14332241043f2fec741cef86a99baf0f440ad56882bb154d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context["grid_size"] = $this->env->getExtension('Grav\Common\Twig\TwigExtension')->themeVarFunc("grid-size");
        // line 2
        $context["image"] = twig_first($this->env, $this->getAttribute($this->getAttribute(($context["page"] ?? null), "media", array()), "images", array()));
        // line 3
        echo "
<section class=\"section ";
        // line 4
        echo $this->getAttribute($this->getAttribute(($context["page"] ?? null), "header", array()), "class", array());
        echo "\" id=\"";
        echo $this->getAttribute($this->getAttribute(($context["page"] ?? null), "header", array()), "id", array());
        echo "\">
  <section class=\"container ";
        // line 5
        echo ($context["grid_size"] ?? null);
        echo "\">
    ";
        // line 6
        echo ($context["content"] ?? null);
        echo "
  </section>
</section>
";
    }

    public function getTemplateName()
    {
        return "modular/section.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  36 => 6,  32 => 5,  26 => 4,  23 => 3,  21 => 2,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% set grid_size = theme_var('grid-size') %}
{% set image = page.media.images|first %}

<section class=\"section {{ page.header.class}}\" id=\"{{ page.header.id}}\">
  <section class=\"container {{ grid_size }}\">
    {{content}}
  </section>
</section>
", "modular/section.html.twig", "/var/www/html/permit-experts-v2/user/themes/permit-experts/templates/modular/section.html.twig");
    }
}
