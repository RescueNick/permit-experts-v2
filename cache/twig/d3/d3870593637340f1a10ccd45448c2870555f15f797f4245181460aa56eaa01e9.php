<?php

/* partials/navigation.html.twig */
class __TwigTemplate_022400b9cd3fef781592f80fe59a147247d00fb6e20189b95d7a5322a53ddd40 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 22
        echo "
<ul>
<li><a href=\"tel:17803946187\"><span class=\"fa fa-phone\"></span>1-780-394-6187</a></li>
<li><a href=\"mailto:info@permitexperts.ca\"><span class=\"fa fa-envelope\"></span>info@permitexperts.ca</a></li>
</ul>
<ul ";
        // line 27
        echo ((($context["tree"] ?? null)) ? ("class=\"tree\"") : (""));
        echo ">
";
        // line 28
        echo $this->getAttribute($this, "nav_loop", array(0 => ($context["pages"] ?? null)), "method");
        echo "
</ul>

";
    }

    // line 1
    public function getnav_loop($__page__ = null, ...$__varargs__)
    {
        $context = $this->env->mergeGlobals(array(
            "page" => $__page__,
            "varargs" => $__varargs__,
        ));

        $blocks = array();

        ob_start();
        try {
            // line 2
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute(($context["page"] ?? null), "children", array()), "visible", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["p"]) {
                // line 3
                echo "    ";
                $context["active_page"] = ((($this->getAttribute($context["p"], "active", array()) || $this->getAttribute($context["p"], "activeChild", array()))) ? ("active") : (""));
                // line 4
                echo "    ";
                if (($this->getAttribute($this->getAttribute($this->getAttribute($context["p"], "children", array()), "visible", array()), "count", array()) > 0)) {
                    // line 5
                    echo "        <li>
            <a href=\"";
                    // line 6
                    echo $this->getAttribute($context["p"], "url", array());
                    echo "\" class=\"";
                    echo ($context["active_page"] ?? null);
                    echo "\">
                ";
                    // line 7
                    echo $this->getAttribute($context["p"], "menu", array());
                    echo "
            </a>
            <ul>
                ";
                    // line 10
                    echo $this->getAttribute($this, "nav_loop", array(0 => $context["p"]), "method");
                    echo "
            </ul>
        </li>
    ";
                } else {
                    // line 14
                    echo "        <li>
            <a href=\"";
                    // line 15
                    echo $this->getAttribute($context["p"], "url", array());
                    echo "\" class=\"";
                    echo ($context["active_page"] ?? null);
                    echo "\">
                ";
                    // line 16
                    echo $this->getAttribute($context["p"], "menu", array());
                    echo "
            </a>
        </li>
    ";
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['p'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        } catch (Exception $e) {
            ob_end_clean();

            throw $e;
        } catch (Throwable $e) {
            ob_end_clean();

            throw $e;
        }

        return ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
    }

    public function getTemplateName()
    {
        return "partials/navigation.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  91 => 16,  85 => 15,  82 => 14,  75 => 10,  69 => 7,  63 => 6,  60 => 5,  57 => 4,  54 => 3,  50 => 2,  38 => 1,  30 => 28,  26 => 27,  19 => 22,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% macro nav_loop(page) %}
{% for p in page.children.visible %}
    {% set active_page = (p.active or p.activeChild) ? 'active' : '' %}
    {% if p.children.visible.count > 0 %}
        <li>
            <a href=\"{{ p.url }}\" class=\"{{ active_page }}\">
                {{ p.menu }}
            </a>
            <ul>
                {{ _self.nav_loop(p) }}
            </ul>
        </li>
    {% else %}
        <li>
            <a href=\"{{ p.url }}\" class=\"{{ active_page }}\">
                {{ p.menu }}
            </a>
        </li>
    {% endif %}
{% endfor %}
{% endmacro %}

<ul>
<li><a href=\"tel:17803946187\"><span class=\"fa fa-phone\"></span>1-780-394-6187</a></li>
<li><a href=\"mailto:info@permitexperts.ca\"><span class=\"fa fa-envelope\"></span>info@permitexperts.ca</a></li>
</ul>
<ul {{ tree ? 'class=\"tree\"' : '' }}>
{{ _self.nav_loop(pages) }}
</ul>

", "partials/navigation.html.twig", "/var/www/html/permit-experts-v2/user/themes/permit-experts/templates/partials/navigation.html.twig");
    }
}
