<?php

/* @images/vertical_logo.svg */
class __TwigTemplate_2a357f5abe2ca45de97f5e702778d5592165f29de4bdae616b3e42dd3e2c5bed extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<?xml version=\"1.0\" encoding=\"utf-8\" standalone=\"yes\"?>
<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.1//EN\" \"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd\">
<svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:dc=\"http://purl.org/dc/elements/1.1/\" xmlns:cc=\"http://web.resource.org/cc/\" xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\" xmlns:sodipodi=\"http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd\" xmlns:inkscape=\"http://www.inkscape.org/namespaces/inkscape\" version=\"1.1\" baseProfile=\"full\" width=\"150px\" height=\"100px\" viewBox=\"0 0 150 100\" preserveAspectRatio=\"xMidYMid meet\" id=\"svg_document\" style=\"zoom: 1;\"><title id=\"svg_document_title\">Untitled.svg</title><defs id=\"svg_document_defs\"><style id=\"Orbitron_Google_Webfont_import\">@import url(https://fonts.googleapis.com/css?family=Orbitron);</style></defs><g id=\"main_group\"><text id=\"sample_text_element\" style=\"outline-style:none;\" xml:space=\"preserve\" x=\"70px\" font-weight=\"500\" text-rendering=\"geometricPrecision\" font-family=\"Orbitron\" y=\"42px\" fill=\"#1c964c\" font-size=\"28px\" transform=\"\" text-anchor=\"middle\">PERMIT</text></g><text stroke=\"none\" id=\"text3\" style=\"outline-style:none;\" stroke-width=\"1px\" x=\"69px\" text-rendering=\"geometricPrecision\" font-family=\"Helvetica\" fill=\"#000000\" font-size=\"32px\" y=\"92px\" transform=\"\" text-anchor=\"middle\"></text><text stroke=\"none\" style=\"outline-style:none;\" id=\"text1\" stroke-width=\"1px\" x=\"74px\" font-weight=\"500\" text-rendering=\"geometricPrecision\" font-family=\"Helvetica\" fill=\"#1c964c\" font-size=\"28px\" y=\"64px\" transform=\"\" text-anchor=\"middle\">EXPERTS</text><text stroke=\"none\" style=\"outline-style:none;\" id=\"text2\" stroke-width=\"1px\" x=\"102px\" text-rendering=\"geometricPrecision\" font-family=\"Helvetica\" fill=\"#000000\" font-size=\"14px\" y=\"78px\" transform=\"\" text-anchor=\"middle\">LIFE IS GOOD</text></svg>";
    }

    public function getTemplateName()
    {
        return "@images/vertical_logo.svg";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?xml version=\"1.0\" encoding=\"utf-8\" standalone=\"yes\"?>
<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.1//EN\" \"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd\">
<svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:dc=\"http://purl.org/dc/elements/1.1/\" xmlns:cc=\"http://web.resource.org/cc/\" xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\" xmlns:sodipodi=\"http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd\" xmlns:inkscape=\"http://www.inkscape.org/namespaces/inkscape\" version=\"1.1\" baseProfile=\"full\" width=\"150px\" height=\"100px\" viewBox=\"0 0 150 100\" preserveAspectRatio=\"xMidYMid meet\" id=\"svg_document\" style=\"zoom: 1;\"><title id=\"svg_document_title\">Untitled.svg</title><defs id=\"svg_document_defs\"><style id=\"Orbitron_Google_Webfont_import\">@import url(https://fonts.googleapis.com/css?family=Orbitron);</style></defs><g id=\"main_group\"><text id=\"sample_text_element\" style=\"outline-style:none;\" xml:space=\"preserve\" x=\"70px\" font-weight=\"500\" text-rendering=\"geometricPrecision\" font-family=\"Orbitron\" y=\"42px\" fill=\"#1c964c\" font-size=\"28px\" transform=\"\" text-anchor=\"middle\">PERMIT</text></g><text stroke=\"none\" id=\"text3\" style=\"outline-style:none;\" stroke-width=\"1px\" x=\"69px\" text-rendering=\"geometricPrecision\" font-family=\"Helvetica\" fill=\"#000000\" font-size=\"32px\" y=\"92px\" transform=\"\" text-anchor=\"middle\"></text><text stroke=\"none\" style=\"outline-style:none;\" id=\"text1\" stroke-width=\"1px\" x=\"74px\" font-weight=\"500\" text-rendering=\"geometricPrecision\" font-family=\"Helvetica\" fill=\"#1c964c\" font-size=\"28px\" y=\"64px\" transform=\"\" text-anchor=\"middle\">EXPERTS</text><text stroke=\"none\" style=\"outline-style:none;\" id=\"text2\" stroke-width=\"1px\" x=\"102px\" text-rendering=\"geometricPrecision\" font-family=\"Helvetica\" fill=\"#000000\" font-size=\"14px\" y=\"78px\" transform=\"\" text-anchor=\"middle\">LIFE IS GOOD</text></svg>", "@images/vertical_logo.svg", "/Users/nick/Dropbox/Development/Permit_Experts_v2/user/themes/permit-experts/images/vertical_logo.svg");
    }
}
